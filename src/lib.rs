use rand::{distributions::Distribution, Rng};
use rand_distr::{Normal, StandardNormal};

#[derive(Debug, Clone, PartialEq)]
pub struct Oscillator<R, VD, AD, JD> {
    pub rng: R,
    pub x: f64,
    pub v: f64,
    pub rv: VD,
    pub a: f64,
    pub ra: AD,
    pub rj: JD,
}

impl<R> Oscillator<R, StandardNormal, Normal<f64>, Normal<f64>> {
    /// Generate a test oscillator
    pub fn test_oscillator(
        rng: R,
        x: f64,
        v: f64,
        a: f64,
    ) -> Oscillator<R, StandardNormal, Normal<f64>, Normal<f64>> {
        Oscillator {
            rng,
            x,
            v,
            a,
            rv: StandardNormal,
            ra: Normal::new(0.0, 0.1).unwrap(),
            rj: Normal::new(0.0, 0.01).unwrap(),
        }
    }
}

impl<R, VD, AD, JD> Oscillator<R, VD, AD, JD>
where
    R: Rng,
    VD: Distribution<f64>,
    AD: Distribution<f64>,
    JD: Distribution<f64>,
{
    /// Go forwards a time-step
    pub fn forward(&mut self) {
        let dx = self.v + self.rv.sample(&mut self.rng);
        let dv = self.a + self.ra.sample(&mut self.rng);
        let da = self.rj.sample(&mut self.rng);
        self.x += dx;
        self.v += dv;
        self.a += da;
    }
    /// Go forwards a time-step, returning the current value
    pub fn next_x(&mut self) -> f64 {
        let result = self.x;
        self.forward();
        result
    }
}

impl<R, VD, AD, JD> Iterator for Oscillator<R, VD, AD, JD>
where
    R: Rng,
    VD: Distribution<f64>,
    AD: Distribution<f64>,
    JD: Distribution<f64>,
{
    type Item = f64;
    fn next(&mut self) -> Option<f64> {
        Some(self.next_x())
    }
}
