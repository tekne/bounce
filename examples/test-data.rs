use bounce::Oscillator;
use clap::{App, Arg};
use rand::thread_rng;

pub fn main() {
    let matches = App::new("Test Data Generator")
        .version("1.0")
        .author("Jad Ghalayini")
        .about("Generates test oscillator data")
        .arg(
            Arg::with_name("iterations")
                .short("n")
                .long("iterations")
                .help("Sets a custom config file")
                .takes_value(true),
        )
        .get_matches();
    let n = matches
        .value_of("iterations")
        .map(|iters| iters.parse::<u64>().expect("Positive value for iterations"))
        .unwrap_or(1000);
    let mut oscillator = Oscillator::test_oscillator(thread_rng(), 50.0, 0.001, 0.001);
    oscillator.forward();
    println!("x, v, a");
    for _ in 0..n {
        println!("{}, {}, {}", oscillator.x, oscillator.v, oscillator.a);
        oscillator.forward();
    }
}
